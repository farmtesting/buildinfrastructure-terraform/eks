module "ec2" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = ">= 2.0"

  name                   = "bastion"
  instance_count         = 1
  # Public Image Ubuntu 10.04-1 16.2
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t3.medium"
  #   key_name               = "admin-ssh"
  monitoring             = true
  vpc_security_group_ids = [ aws_security_group.bastion.id ]
  subnet_id              = module.vpc.public_subnets[0]

  user_data = data.template_file.init-bastion-script.rendered
  associate_public_ip_address = true

  tags = {
    Name = "bastion"
    Terraform   = "true"
    Environment = var.environment_tag
  }
}

# Template for initial configuration bash script
data "template_file" "init-bastion-script" {
  template = file("scripts/installBastion.sh")
}

data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["258751437250"]

  filter {
    name   = "name"
    values = ["ami-ubuntu-18.04-1.13*"]
  }
}

# Security Groups
######
resource "aws_security_group" "bastion" {
  name = "sg_bastion"
  description         = "Allow HTTP and SSH inbound traffic."
  vpc_id              = module.vpc.vpc_id

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Http and https access
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]

  }

  tags = {
    Environment         = var.environment_tag
    Name                = "sg-bastion"
  }

}