data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

data "aws_availability_zones" "available" {}

data "aws_caller_identity" "current" {}

module "vpc" {
  source               = "terraform-aws-modules/vpc/aws"
  // version              = "2.6.0"
  name                 = local.cluster_name
  cidr                 = "192.168.0.0/16"
  azs                  = data.aws_availability_zones.available.names
  public_subnets       = ["192.168.11.0/24", "192.168.12.0/24", "192.168.13.0/24"]
  private_subnets      = ["192.168.121.0/24", "192.168.122.0/24", "192.168.123.0/24"]
  enable_dns_hostnames = true

  public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"              = "1"
  }

}

module "eks" {
  source       = "terraform-aws-modules/eks/aws"
  cluster_version = "1.16"
  cluster_name = local.cluster_name
  subnets      = module.vpc.public_subnets
  vpc_id       = module.vpc.vpc_id
  enable_irsa  = true

  worker_groups = [
    {
      name                 = "worker-group-1"
      instance_type        = "t3.medium"
      asg_desired_capacity  = 2
      asg_min_size          = 2
      asg_max_size          = 5
      autoscaling_enabled   = true
      protect_from_scale_in = true
      
      tags = [
        {
          "key"                 = "k8s.io/cluster-autoscaler/enabled"
          "propagate_at_launch" = "true"
          "value"               = "true"
        },
        {
          "key"                 = "k8s.io/cluster-autoscaler/${local.cluster_name}"
          "propagate_at_launch" = "false"
          "value"               = "true"
        }
      ]
    }
  ]
}
