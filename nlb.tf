##################################################################
# Network Load Balancer with Elastic IPs attached
##################################################################
module "nlb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 5.0"

  name = var.name_nlb_nodes

  load_balancer_type = "network"

  vpc_id = module.vpc.vpc_id
  subnets            = module.vpc.public_subnets
#   security_groups    = [ module.security_group.this_security_group_id ]
  
  //  Use `subnets` if you don't want to attach EIPs
  //  subnets = tolist(data.aws_subnet_ids.all.ids)

  //  Use `subnet_mapping` to attach EIPs
  // subnet_mapping = [for i, eip in aws_eip.this : { allocation_id : eip.id, subnet_id : tolist(data.aws_subnet_ids.all.ids)[i] }]

  //  # See notes in README (ref: https://github.com/terraform-providers/terraform-provider-aws/issues/7987)
  //  access_logs = {
  //    bucket = module.log_bucket.this_s3_bucket_id
  //  }


  // TCP_UDP, UDP, TCP
  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "TCP"
      target_group_index = 0
    }
  ]

  target_groups = [
    {
      backend_protocol = "TCP"
      backend_port     = 80
      target_type      = "instance"
    }
  ]

  tags = {
    Environment = var.environment_tag
    Project = "eks-cluster"
  }
}