locals {
  cluster_name                  = "EKS_CLUSTER_NAME"
  k8s_service_account_namespace = "kube-system"
  k8s_service_account_name      = "cluster-autoscaler-aws-cluster-autoscaler"
}

variable "region" {
  default = "eu-west-3"
}

variable "environment_tag" {
  default = "prod"
}

variable "name_alb_nodes" {
  default = "eks-lb"
}

variable "name_nlb_nodes" {
  default = "eks-nlb"
}