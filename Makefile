SHELL := /usr/bin/env bash
# All is the first target in the file so it will get picked up when you just run 'make' on its own

all-test: clean tf-plan

.PHONY: clean
clean:
	rm -rf .terraform

.PHONY: tf-plan
tf-plan:
	terraform init && terraform validate && terraform plan 

.PHONY: tf-apply
tf-apply:
	terraform init && terraform validate && terraform apply -auto-approve

.PHONY: tf-destroy
tf-destroy:
	terraform init && terraform validate && terraform destroy -auto-approve