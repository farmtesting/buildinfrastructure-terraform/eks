#!/bin/bash

mkdir temporary && cd temporary

#### Install DOCKER CE ####
# PREREQUIES
apt update -y 
apt install -y curl git make vim 

#### Install Ansible ####
apt-add-repository ppa:ansible/ansible -y 
apt-get update
apt-get install ansible -y

#### Install Docker CE #### 
apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt update -y 
apt-cache policy docker-ce
apt install docker-ce -y
# ENABLE AND START DOCKER CE
systemctl enable docker
systemctl start docker
systemctl status docker

#### Install Kubernetes cli ####
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.16.0/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl

#### Install Helm cli ####
curl -LO https://get.helm.sh/helm-v3.2.4-linux-arm.tar.gz
tar -zxvf helm-v3.2.4-linux-amd64.tar.gz
sudo mv linux-amd64/helm /usr/local/bin/helm
helm repo add stable https://kubernetes-charts.storage.googleapis.com/ # Add Stable repo
helm repo update # Make sure we get the latest list of charts

# Clean repository
cd .. && rm -fr temporary