## Introduction 

This terraform code will deploy into your aws account : 
-   An **EKS cluster** 
-   A **bastion** with preinstalled tools such as aws cli, kubectl 

## Prequisite

- A public **ssh key** imported within your account into the desired region 
- Your **aws credentials** configured as variables


## Deployment 
Two ways for building the infrastructure :
1.   Triggering the gitlab-ci Pipeline. 
2.   Makefile

### The CICD Pipeline

| Step | Launch | Description |
| ------ | ----------- | ----------- |
| init | Auto | Initialize a working directory containing Terraform configuration files.s  |
| plan | Auto |Create an execution plan | 
| apply | Manually | Apply the changes required to reach the desired state of the configuration | 
| destroy | Manually | Destroy the Terraform-managed infrastructure | 

### Makefile

make commands are exectued manually. You can then deploy or destroy the infrastructure.  

| Command | Description |
| ------ | ----------- |
| make clean | Clean the .terraform repository  |
| make tf-plan | Create an execution plan | 
| make tf-apply | Apply the changes required to reach the desired state of the configuration | 
| make tf-destroy | Destroy the Terraform-managed infrastructure | 

